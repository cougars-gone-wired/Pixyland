package frc.robot;

import io.github.pseudoresonance.pixy2api.Pixy2;
// import io.github.pseudoresonance.pixy2api.links.Link;
import io.github.pseudoresonance.pixy2api.links.SPILink;

public class HelloWorld {

	private static Pixy2 pixy;

    public HelloWorld() {
		System.out.println("Initializing HelloWorld");

        pixy = Pixy2.createInstance(new SPILink()); // Creates a new Pixy2 camera using SPILink
		pixy.init(); // Initializes the camera and prepares to send/receive data
		// pixy.setLamp((byte) 1, (byte) 1); // Turns the LEDs on
		// pixy.setLED(255, 120, 0); // Sets the RGB LED to purple

		System.out.println("Initialized HelloWorld");
	}
}
