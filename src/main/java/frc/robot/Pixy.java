package frc.robot;

import java.util.ArrayList;
import java.lang.Math;

import io.github.pseudoresonance.pixy2api.Pixy2;
// import io.github.pseudoresonance.pixy2api.links.Link;
import io.github.pseudoresonance.pixy2api.links.SPILink;
import io.github.pseudoresonance.pixy2api.Pixy2CCC;
import io.github.pseudoresonance.pixy2api.Pixy2CCC.Block;

public class Pixy {

	private static Pixy2 pixy;

    public Pixy() {
		System.out.println("Initializing Pixy SPI Link");

        pixy = Pixy2.createInstance(new SPILink()); // Creates a new Pixy2 camera using SPILink
		pixy.init(); // Initializes the camera and prepares to send/receive data
		// pixy.setLamp((byte) 1, (byte) 1); // Turns the LEDs on
		// pixy.setLED(255, 120, 0); // Sets the RGB LED to purple

		System.out.println("Initialized Pixy SPI Link");
    }
    
    public Block getLargestBlock() {
        // Original example code from https://github.com/PseudoResonance/Pixy2JavaAPI/wiki/Using-the-API#finding-the-biggest-target-in-the-frame

        // Gets the number of "blocks", identified targets, that match signature 1 on the Pixy2,
		// does not wait for new data if none is available,
		// and limits the number of returned blocks to 25, for a slight increase in efficiency
		int blockCount = pixy.getCCC().getBlocks(false, Pixy2CCC.CCC_SIG1, 25);
		// System.out.println("Found " + blockCount + " blocks!"); // Reports number of blocks found
		if (blockCount <= 0) {
			return null; // If blocks were not found, stop processing
		}
		ArrayList<Block> blocks = pixy.getCCC().getBlockCache(); // Gets a list of all blocks found by the Pixy2
		Block largestBlock = null;
		for (Block block : blocks) { // Loops through all blocks and finds the widest one
			if (largestBlock == null) {
				largestBlock = block;
			} else if (block.getWidth() > largestBlock.getWidth()) {
				largestBlock = block;
			}
		}
		return largestBlock;
	}
	
	public double getTargetAngle(Block object){ // Returns the angle relative to Pixy's center
		
		if (object == null){
			return -500;
		}

		// double fov_angle = 60.51;
		double half_fov_pixels = 157.5;
		double object_pixels = 0;
		double angle = 0;
		int raw_x_pixel = object.getX();

		object_pixels = raw_x_pixel-half_fov_pixels;
		angle = Math.toDegrees(Math.atan(object_pixels/270.016)); 

		return angle;
	}

    // public double getObjectDistance(Block objectBlock){
	// 	int xpos = 0;
	// 	int ypos = 0;

	// 	int canvaswidth = 0;
	// 	int canvasheight = 0;
	// 	int pixycam_height = 0;
	// 	int camera_angle = 0;
	// 	int object_angle = 0;

	// 	distance = (object_height-pixycam_height)/tan(camera_angle+object_angle);

	// 	return distance;
    // }
    
}